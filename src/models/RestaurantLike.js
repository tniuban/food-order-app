const { DataTypes, Sequelize } = require('sequelize');

module.exports = (sequelize) =>
  sequelize.define(
    'RestaurantLike',
    {
      userId: {
        type: DataTypes.INTEGER,
        field: 'user_id',
      },
      restaurantId: {
        type: DataTypes.INTEGER,
        field: 'restaurant_id',
      },
      date_like: {
        type: DataTypes.DATE,
        field: 'date_like',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    },
    {
      tableName: 'restaurant_likes',
      timestamps: false,
    }
  );

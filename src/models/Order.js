const { DataTypes } = require('sequelize');

module.exports = (sequelize) =>
  sequelize.define(
    'Order',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      userId: {
        type: DataTypes.INTEGER,
        field: 'user_id',
        allowNull: false,
      },
      foodId: {
        type: DataTypes.INTEGER,
        field: 'food_id',
        allowNull: false,
      },
      amount: {
        type: DataTypes.INTEGER,
      },
      code: {
        type: DataTypes.STRING(255),
      },
      arrSubId: {
        type: DataTypes.STRING(255),
        field: 'arr_sub_id',
      },
    },
    {
      tableName: 'orders',
      timestamps: false,
    }
  );

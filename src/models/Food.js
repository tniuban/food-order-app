const { DataTypes } = require('sequelize');

module.exports = (sequelize) =>
  sequelize.define(
    'Food',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING(255),
      },
      image: {
        type: DataTypes.STRING(255),
      },
      price: {
        type: DataTypes.INTEGER,
      },
      desc: {
        type: DataTypes.STRING(255),
      },
      typeId: {
        type: DataTypes.INTEGER,
        field: 'type_id',
        allowNull: false,
      },
    },
    {
      tableName: 'foods',
      timestamps: false,
    }
  );

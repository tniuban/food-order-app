const { SugarError } = require('../../helpers/errors');
const { Restaurant, User } = require('../../models');
const {
  ErrUserNotFound,
  ErrRestaurantNotFound,
} = require('../../pkg/appError');

module.exports = async ({ userId, restaurantId, amount }) => {
  try {
    const restaurant = await Restaurant.findByPk(restaurantId);
    if (!restaurant) {
      throw new SugarError(ErrRestaurantNotFound);
    }

    const user = await User.findOne({
      where: { id: userId },
    });
    if (!user) {
      throw new SugarError(ErrUserNotFound);
    }

    await restaurant.addUserRate(userId, { through: { amount } });
    return true;
  } catch (error) {
    throw new SugarError(error);
  }
};

const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');
const {
  ErrRestaurantNotFound,
  ErrPermissionDenied,
} = require('../../pkg/appError');

module.exports = async ({ id, requesterId }) => {
  try {
    const restaurant = await Restaurant.findOne({ where: { id } });

    if (!restaurant) {
      throw new SugarError(ErrRestaurantNotFound);
    }

    // Check restaurant owner
    if (restaurant.ownerId !== requesterId) {
      throw new SugarError(ErrPermissionDenied);
    }

    await Restaurant.destroy({ where: { id } });
    return true;
  } catch (error) {
    throw new SugarError(error);
  }
};

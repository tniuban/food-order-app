const { SugarError } = require('../../helpers/errors');
const { Restaurant, User } = require('../../models');
const {
  ErrRestaurantNotFound,
  ErrUserNotFound,
  ErrPermissionDenied,
} = require('../../pkg/appError');

module.exports = async (id, data, requesterId) => {
  try {
    const restaurant = await Restaurant.findOne({
      where: {
        id: id,
      },
    });

    if (!restaurant) {
      throw new SugarError(ErrRestaurantNotFound);
    }

    // Check restaurant owner
    if (restaurant.ownerId !== requesterId) {
      throw new SugarError(ErrPermissionDenied);
    }

    // Check user existed if change owner
    if (data.ownerId) {
      const user = await User.findOne({ where: { id: ownerId } });
      if (!user) {
        throw new SugarError(ErrUserNotFound);
      }
    }

    restaurant.set(data);
    await restaurant.save();

    return restaurant;
  } catch (error) {
    throw new SugarError(error);
  }
};

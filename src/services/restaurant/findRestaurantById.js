const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');
const { ErrRestaurantNotFound } = require('../../pkg/appError');

module.exports = async (id) => {
	try {
		const restaurant = await Restaurant.findOne({ where: { id } });

		if (!restaurant) {
      throw new SugarError(ErrRestaurantNotFound);
		}

		return restaurant;
	} catch (error) {
		throw new SugarError(error);
	}
};

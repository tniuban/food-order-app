const { SugarError } = require('../../helpers/errors');
const { RestaurantRate } = require('../../models');
const R = require('ramda');

module.exports = async ({ userId, restaurantId, limit = 5, offset = 0 }) => {
  const filter = R.reject(R.isNil, { userId, restaurantId });
  try {
    const restaurantRates = await RestaurantRate.findAll({
      where: filter,
      limit: limit + 1,
      offset,
    });

    let nextPagination = null;

    if (restaurantRates.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      restaurantRates.pop();
    }

    return { restaurantRates, nextPagination };
  } catch (error) {
    throw new SugarError(error);
  }
};

const { SugarError } = require('../../helpers/errors');
const { Restaurant, User } = require('../../models');
const { ErrUserNotFound } = require('../../pkg/appError');

module.exports = async ({ name, description, ownerId }) => {
  try {
    const restaurant = await Restaurant.create({ name, description, ownerId });
    return restaurant;
  } catch (error) {
    throw new SugarError(error);
  }
};

const { SugarError } = require('../../helpers/errors');
const { Restaurant } = require('../../models');

module.exports = async ({ limit, offset }) => {
	try {
		const restaurants = await Restaurant.findAll({
      limit: limit + 1,
      offset,
    });

		let nextPagination = null;

    if (restaurants.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      restaurants.pop();
    }

		return { restaurants, nextPagination };
	} catch (error) {
		throw new SugarError(error);
	}
};

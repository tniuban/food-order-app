const { SugarError } = require('../../helpers/errors');
const { Restaurant, User } = require('../../models');
const {
  ErrRestaurantNotFound,
  ErrUserNotFound,
} = require('../../pkg/appError');

module.exports = async ({ userId, restaurantId }) => {
  try {
    const restaurant = await Restaurant.findOne({
      where: { id: restaurantId },
    });
    if (!restaurant) {
      throw new SugarError(ErrRestaurantNotFound);
    }

    const user = await User.findOne({
      where: { id: userId },
    });
    if (!user) {
      throw new SugarError(ErrUserNotFound);
    }

    const hasLiked = await restaurant.hasUserLike(userId);

    if (hasLiked) {
      await restaurant.removeUserLike(userId);
      return 'disliked';
    } else {
      await restaurant.addUserLike(userId);
      return 'liked';
    }
  } catch (error) {
    throw new SugarError(error);
  }
};

const { SugarError } = require('../../helpers/errors');
const { Food } = require('../../models');
const { ErrFoodNotFound } = require('../../pkg/appError');

module.exports = async (id) => {
	try {
		const food = await Food.findOne({ where: { id } });

		if (!food) {
      throw new SugarError(ErrFoodNotFound);
		}
	
		await Food.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);
	}
};

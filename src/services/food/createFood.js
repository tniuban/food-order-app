const { SugarError } = require('../../helpers/errors');
const { Food, FoodType } = require('../../models');

module.exports = async (args) => {
  try {
    const { typeId } = args;
    const foodType = await FoodType.findOne({ where: { id: typeId } });

    if (!foodType) {
      throw new SugarError(ErrFoodTypeNotFound);
    }

    const food = await Food.create(args);
    return food;
  } catch (error) {
    throw new SugarError(error);
  }
};

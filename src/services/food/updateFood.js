const { SugarError } = require('../../helpers/errors');
const { Food } = require('../../models');
const { ErrFoodNotFound } = require('../../pkg/appError');

module.exports = async (id, args) => {
	try {
		const food = await Food.findOne({
      where: {
        id: id,
      },
    });

    if (!food) {
      throw new SugarError(ErrFoodNotFound);
    }

    food.set(args);
    await food.save();

		return food;
	} catch (error) {
		throw new SugarError(error);
	}
};

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../../config');
const { SugarError } = require('../../helpers/errors');
const { User } = require('../../models');
const { ErrLogin } = require('../../pkg/appError');

module.exports = async ({ email, password }) => {
  try {
    const user = await User.findOne({
      where: { email },
      include: {
        association: 'restaurants',
        attributes: { exclude: 'ownerId' },
      },
      attributes: {
        include: ['password'],
      },
    });

    if (!user) {
      throw new SugarError(ErrLogin);
    }

    const isMatchedPassword = bcrypt.compare(password, user.password);

    if (!isMatchedPassword) {
      throw new SugarError(ErrLogin);
    }
    delete user.dataValues.password;

    const restaurantIds = user.restaurants.map(({ id }) => id);
    const jwtData = { id: user.id, restaurantIds };

    const accessToken = jwt.sign(jwtData, config.jwtSecretKey, {
      expiresIn: 60 * 60 * 24, // 24 hours
    });

    const refreshToken = jwt.sign(jwtData, config.jwtSecretKey, {
      expiresIn: 60 * 60 * 24 * 30, // 1 month
    });

    return { profile: user, accessToken, refreshToken };
  } catch (error) {
    throw new SugarError(error);
  }
};

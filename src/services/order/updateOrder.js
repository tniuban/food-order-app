const { SugarError } = require('../../helpers/errors');
const { Order, Food, User } = require('../../models');
const {
  ErrOrderNotFound,
  ErrFoodNotFound,
  ErrPermissionDenied,
} = require('../../pkg/appError');

module.exports = async (
  id,
  // Do not allow to change userId
  { userId, ...args },
  requesterId
) => {
  try {
    const order = await Order.findOne({
      where: {
        id: id,
      },
    });

    if (!order) {
      throw new SugarError(ErrOrderNotFound);
    }

    // Check order owner
    if (order.userId !== requesterId) {
      throw new SugarError(ErrPermissionDenied);
    }

    // Check food exist
    const { foodId } = args;
    const food = await Food.findOne({ where: { id: foodId } });
    if (!food) {
      throw new SugarError(ErrFoodNotFound);
    }

    order.set(args);
    await order.save();

    return order;
  } catch (error) {
    throw new SugarError(error);
  }
};

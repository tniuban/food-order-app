const { SugarError } = require('../../helpers/errors');
const { Order } = require('../../models');
const { ErrOrderNotFound } = require('../../pkg/appError');

module.exports = async (id) => {
  try {
    const order = await Order.findOne({
      where: { id },
      include: ['food', 'user'],
    });

    if (!order) {
      throw new SugarError(ErrOrderNotFound);
    }

    return order;
  } catch (error) {
    throw new SugarError(error);
  }
};

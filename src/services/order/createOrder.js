const { SugarError } = require('../../helpers/errors');
const { Order, Food, User } = require('../../models');
const { ErrFoodNotFound, ErrUserNotFound } = require('../../pkg/appError');

module.exports = async ({ userId, foodId, amount, code, arrSubId }) => {
  console.log('first', userId, foodId, amount, code, arrSubId);
  try {
    const user = await User.findOne({ where: { id: userId } });
    if (!user) {
      throw new SugarError(ErrUserNotFound);
    }

    const food = await Food.findOne({ where: { id: foodId } });
    if (!food) {
      throw new SugarError(ErrFoodNotFound);
    }

    const order = await Order.create({
      userId,
      foodId,
      amount,
      code,
      arrSubId,
    });
    return order;
  } catch (error) {
    throw new SugarError(error);
  }
};

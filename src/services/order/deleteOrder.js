const { SugarError } = require('../../helpers/errors');
const { Order } = require('../../models');
const { ErrOrderNotFound, ErrPermissionDenied } = require('../../pkg/appError');

module.exports = async (id, requesterId) => {
  try {
    const order = await Order.findOne({ where: { id } });

    if (!order) {
      throw new SugarError(ErrOrderNotFound);
    }

    // Check order owner
    if (order.userId !== requesterId) {
      throw new SugarError(ErrPermissionDenied);
    }

    await Order.destroy({ where: { id } });
    return true;
  } catch (error) {
    throw new SugarError(error);
  }
};

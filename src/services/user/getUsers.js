const { SugarError } = require('../../helpers/errors');
const { User } = require('../../models');

module.exports = async ({ limit, offset }) => {
  try {
    const users = await User.findAll({
      include: {
        association: 'restaurants',
        attributes: {
          exclude: ['ownerId'],
        },
      },
      limit: limit + 1,
      offset,
    });

    let nextPagination = null;

    if (users.length > limit) {
      nextPagination = { limit, offset: offset + limit };
      users.pop();
    }

    return { users, nextPagination };
  } catch (error) {
    throw new SugarError(error);
  }
};

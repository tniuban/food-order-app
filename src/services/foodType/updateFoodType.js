const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');
const { ErrFoodTypeNotFound } = require('../../pkg/appError');

module.exports = async (id, args) => {
	try {
		const foodType = await FoodType.findOne({
      where: {
        id: id,
      },
    });

    if (!foodType) {
      throw new SugarError(ErrFoodTypeNotFound);
    }

    foodType.set(args);
    await foodType.save();

		return foodType;
	} catch (error) {
		throw new SugarError(error);
	}
};

const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');
const { ErrFoodTypeNotFound } = require('../../pkg/appError');

module.exports = async (id) => {
	try {
		const foodType = await FoodType.findOne({ where: { id } });

		if (!foodType) {
      throw new SugarError(ErrFoodTypeNotFound);
		}
	
		await FoodType.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);
	}
};

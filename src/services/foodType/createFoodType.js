const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');

module.exports = async (args) => {
	try {
		const foodType = await FoodType.create(args);
		return foodType;
	} catch (error) {
    throw new SugarError(error);
	}
};

const { SugarError } = require('../../helpers/errors');
const { FoodType } = require('../../models');
const { ErrFoodTypeNotFound } = require('../../pkg/appError');

module.exports = async (id) => {
	try {
		const foodType = await FoodType.findOne({ where: { id } });

		if (!foodType) {
      throw new SugarError(ErrFoodTypeNotFound);
		}

		return foodType;
	} catch (error) {
		throw new SugarError(error);
	}
};

const { SugarError } = require('../../helpers/errors');
const { SubFood, Food } = require('../../models');

module.exports = async (args) => {
  try {
    const { foodId } = args;
    const food = await Food.findOne({ where: { id: foodId } });

    if (!food) {
      throw new SugarError(ErrFoodNotFound);
    }

    const subFood = await SubFood.create(args);

    return subFood;
  } catch (error) {
    throw new SugarError(error);
  }
};

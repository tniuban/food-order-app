const { SugarError } = require('../../helpers/errors');
const { SubFood } = require('../../models');
const { ErrSubFoodNotFound } = require('../../pkg/appError');

module.exports = async (id) => {
	try {
		const subFood = await SubFood.findOne({ where: { id } });

		if (!subFood) {
      throw new SugarError(ErrSubFoodNotFound);
		}
	
		await SubFood.destroy({ where: { id } });
		return true;
	} catch (error) {
		throw new SugarError(error);
	}
};

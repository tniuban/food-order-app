const { SugarError } = require('../../helpers/errors');
const { SubFood } = require('../../models');
const { ErrSubFoodNotFound } = require('../../pkg/appError');

module.exports = async (id, args) => {
	try {
		const subFood = await SubFood.findOne({
      where: {
        id: id,
      },
    });

    if (!subFood) {
      throw new SugarError(ErrSubFoodNotFound);
    }

    subFood.set(args);
    await subFood.save();

		return subFood;
	} catch (error) {
		throw new SugarError(error);
	}
};

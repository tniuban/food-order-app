const { SugarError } = require('../../helpers/errors');
const resp = require('../../helpers/response');
const { ErrAccountExisted } = require('../../pkg/appError');

const upload = (req, res, next) => {
  const file = req.file;
  if (!file) {
    next(new SugarError(ErrAccountExisted));
  }
  resp({
    code: 0,
    data: file,
  });
};

module.exports = upload;

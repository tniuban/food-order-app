const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { id: restaurantId } = req.params;
  const { userId } = req;
  try {
    const response = await restaurantServices.likeRestaurant({
      userId,
      restaurantId,
    });
    resp({
      res,
      data: response,
    });
  } catch (error) {
    next(error);
  }
};

const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { id: restaurantId } = req.params;
  const { amount } = req.body;
  const { userId } = req;
  try {
    const response = await restaurantServices.rateRestaurant({
      userId,
      restaurantId,
      amount,
    });
    resp({
      res,
      data: response,
    });
  } catch (error) {
    next(error);
  }
};

const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { id } = req.params;
  const { userId } = req;
  try {
    const restaurant = await restaurantServices.deleteRestaurant({
      id,
      requesterId: userId,
    });
    resp({
      res,
      data: restaurant,
    });
  } catch (error) {
    next(error);
  }
};

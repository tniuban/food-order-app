const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { limit, offset } = req.body;
	try {
		const restaurant = await restaurantServices.getRestaurants({ limit, offset });
		resp({
			res,
			data: restaurant,
		});
	} catch (error) {
		next(error);
	}
};

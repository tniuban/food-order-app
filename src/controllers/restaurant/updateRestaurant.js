const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { id } = req.params;
  const { userId, body } = req;
  try {
    const restaurant = await restaurantServices.updateRestaurant(
      id,
      body,
      userId
    );
    resp({
      res,
      data: restaurant,
    });
  } catch (error) {
    next(error);
  }
};

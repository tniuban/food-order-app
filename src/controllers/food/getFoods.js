const resp = require('../../helpers/response');
const foodServices = require('../../services/food');

module.exports = async (req, res, next) => {
  const { limit, offset } = req.body;
	try {
		const food = await foodServices.getFoods({ limit, offset });
		resp({
			res,
			data: food,
		});
	} catch (error) {
		next(error);
	}
};

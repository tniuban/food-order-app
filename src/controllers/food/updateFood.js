const resp = require('../../helpers/response');
const foodServices = require('../../services/food');

module.exports = async (req, res, next) => {
	const { id } = req.params;
	const args = req.body;
	try {
		const food = await foodServices.updateFood(id, args);
		resp({
			res,
			data: food,
		});
	} catch (error) {
		next(error);
	}
};

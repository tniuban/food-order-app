const resp = require('../../helpers/response');
const foodServices = require('../../services/food');

module.exports = async (req, res, next) => {
	const { id } = req.params;
	try {
		const food = await foodServices.findFoodById(id);
		resp({
			res,
			data: food,
		});
	} catch (error) {
		next(error);
	}
};

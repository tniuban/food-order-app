const resp = require('../../helpers/response');
const userServices = require('../../services/user');

module.exports = async (req, res, next) => {
  const { userId } = req;
  try {
    const response = await userServices.findUserById(userId);
    resp({
      res,
      data: response,
    });
  } catch (error) {
    next(error);
  }
};

const authControllers = {
  login: require('./login'),
  getProfile: require('./getProfile'),
};

module.exports = authControllers;

const resp = require('../../helpers/response');
const orderServices = require('../../services/order');

module.exports = async (req, res, next) => {
  const { foodId, amount, code, arrSubId } = req.body;
  const { userId } = req;

  try {
    const order = await orderServices.createOrder({
      userId,
      foodId,
      amount,
      code,
      arrSubId,
    });
    resp({
      res,
      data: order,
    });
  } catch (error) {
    next(error);
  }
};

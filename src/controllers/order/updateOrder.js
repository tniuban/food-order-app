const resp = require('../../helpers/response');
const orderServices = require('../../services/order');

module.exports = async (req, res, next) => {
  const { id } = req.params;
  const args = req.body;
  const { userId } = req;
  try {
    const order = await orderServices.updateOrder(id, args, userId);
    resp({
      res,
      data: order,
    });
  } catch (error) {
    next(error);
  }
};

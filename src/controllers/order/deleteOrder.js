const resp = require('../../helpers/response');
const orderServices = require('../../services/order');

module.exports = async (req, res, next) => {
  const { id } = req.params;
  const { userId } = req;

  try {
    const order = await orderServices.deleteOrder(id,userId);
    resp({
      res,
      data: order,
    });
  } catch (error) {
    next(error);
  }
};

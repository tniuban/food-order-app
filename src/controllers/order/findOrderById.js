const resp = require('../../helpers/response');
const orderServices = require('../../services/order');

module.exports = async (req, res, next) => {
	const { id } = req.params;
	try {
		const order = await orderServices.findOrderById(id);
		resp({
			res,
			data: order,
		});
	} catch (error) {
		next(error);
	}
};

const resp = require('../../helpers/response');
const subFoodServices = require('../../services/subFood');

module.exports = async (req, res, next) => {
  const { limit, offset } = req.body;
	try {
		const subFood = await subFoodServices.getSubFoods({ limit, offset });
		resp({
			res,
			data: subFood,
		});
	} catch (error) {
		next(error);
	}
};

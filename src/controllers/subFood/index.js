const subFoodControllers = {
	createSubFood: require('./createSubFood'),
	getSubFoods: require('./getSubFoods'),
	findSubFoodById: require('./findSubFoodById'),
	updateSubFood: require('./updateSubFood'),
	deleteSubFood: require('./deleteSubFood'),
};

module.exports = subFoodControllers;

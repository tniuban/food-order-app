const resp = require('../../helpers/response');
const subFoodServices = require('../../services/subFood');

module.exports = async (req, res, next) => {
	const { id } = req.params;
	try {
		const subFood = await subFoodServices.findSubFoodById(id);
		resp({
			res,
			data: subFood,
		});
	} catch (error) {
		next(error);
	}
};

const userControllers = {
  createUser: require('./createUser'),
  getUsers: require('./getUsers'),
  findUserById: require('./findUserById'),
  updateUser: require('./updateUser'),
  deleteUser: require('./deleteUser'),
  getUserLikes: require('./getUserLikes'),
  getUserRates: require('./getUserRates'),
};

module.exports = userControllers;

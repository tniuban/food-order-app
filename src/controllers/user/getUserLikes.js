const resp = require('../../helpers/response');
const restaurantServices = require('../../services/restaurant');

module.exports = async (req, res, next) => {
  const { id: userId } = req.params;
  const { limit, offset } = req.body;
  try {
    const restaurants = await restaurantServices.getRestaurantLikes({
      userId,
      limit,
      offset,
    });
    resp({
      res,
      data: restaurants,
    });
  } catch (error) {
    next(error);
  }
};

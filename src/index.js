const express = require('express');
const app = express();
const { sequelize } = require('./models');
const { handleErrors } = require('./helpers/errors');

app.use(express.json());
sequelize.sync({
  alter: true,
  // force: true,
});

// Controllers <-> Services <-> Data Accepcer <-> DB
// Model = Data accepcer + DB

const v1 = require('./routes/v1');

app.use('/api/v1', v1);
// global middleware: Đứng dưới cùng nên tất các api đều di qua middleware này
app.use(handleErrors);

app.listen(4100);

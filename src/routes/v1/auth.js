const authController = require('../../controllers/auth');
const authorize = require('../../middlewares/authorize');

const router = require('express').Router();

router.post('/login', authController.login);
router.get('/get-profile', authorize, authController.getProfile);

module.exports = router;

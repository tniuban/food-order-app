const foodTypeController = require('../../controllers/foodType');
const router = require('express').Router();

router.post('/', foodTypeController.createFoodType);
router.get('/', foodTypeController.getFoodTypes);
router.get('/:id', foodTypeController.findFoodTypeById);
router.put('/:id', foodTypeController.updateFoodType);
router.delete('/:id', foodTypeController.deleteFoodType);

module.exports = router;

const foodController = require('../../controllers/food');
const router = require('express').Router();

router.post('/', foodController.createFood);
router.get('/', foodController.getFoods);
router.get('/:id', foodController.findFoodById);
router.put('/:id', foodController.updateFood);
router.delete('/:id', foodController.deleteFood);

module.exports = router;

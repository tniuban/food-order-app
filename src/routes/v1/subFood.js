const subFoodController = require('../../controllers/subFood');
const router = require('express').Router();
    
router.post('/', subFoodController.createSubFood);
router.get('/', subFoodController.getSubFoods);
router.get('/:id', subFoodController.findSubFoodById);
router.put('/:id', subFoodController.updateSubFood);
router.delete('/:id', subFoodController.deleteSubFood);

module.exports = router;

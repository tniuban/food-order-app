const restaurantController = require('../../controllers/restaurant');
const authorize = require('../../middlewares/authorize');
const router = require('express').Router();

router.post('/', authorize, restaurantController.createRestaurant);
router.get('/', restaurantController.getRestaurants);
router.get('/:id', restaurantController.findRestaurantById);
router.put('/:id', authorize, restaurantController.updateRestaurant);
router.delete('/:id', authorize, restaurantController.deleteRestaurant);

router.get('/:id/like', restaurantController.getRestaurantLikes);
router.post('/:id/like', authorize, restaurantController.likeRestaurant);

router.get('/:id/rate', restaurantController.getRestaurantRates);
router.post('/:id/rate', authorize, restaurantController.rateRestaurant);

module.exports = router;

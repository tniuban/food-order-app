const orderController = require('../../controllers/order');
const authorize = require('../../middlewares/authorize');
const router = require('express').Router();

router.post('/', authorize, orderController.createOrder);
router.get('/', orderController.getOrders);
router.get('/:id', orderController.findOrderById);
router.put('/:id', authorize, orderController.updateOrder);
router.delete('/:id', authorize, orderController.deleteOrder);

module.exports = router;

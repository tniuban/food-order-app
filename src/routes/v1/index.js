const router = require('express').Router();

const authRouter = require('./auth');
const userRouter = require('./user');
const restaurantRouter = require('./restaurant');
const foodRouter = require('./food');
const subFoodRouter = require('./subFood');
const foodTypeRouter = require('./foodType');
const orderRouter = require('./order');
const uploadRouter = require('./upload');

router.use('/auth', authRouter);
router.use('/users', userRouter);
router.use('/restaurants', restaurantRouter);
router.use('/foods', foodRouter);
router.use('/sub-foods', subFoodRouter);
router.use('/food-types', foodTypeRouter);
router.use('/orders', orderRouter);

router.use('/upload', uploadRouter);

module.exports = router;

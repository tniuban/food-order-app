const uploadController = require('../../controllers/upload');
const upload = require('../../middlewares/upload');

const router = require('express').Router();

router.post('/', upload.single('file'), uploadController.upload);

module.exports = router;
